import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBTest {
	Connection connection=null;
	
	
	public DBTest(){}
	
	public boolean doConnection()
	{
		try{
			Class.forName(Data.driverName);
			connection = DriverManager.getConnection(Data.url, Data.username, Data.password);
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found " + e.getMessage());
			return false;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	public static void main(String arg[]){
		DBTest con = new DBTest();
		System.out.println("Connection: " + con.doConnection());
		Statement stmt = null;
		String query = " SELECT * FROM szkolenia";
		try {
			stmt=con.connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				String test = rs.getString(1);
				System.out.println(test+" ");
			}
			stmt.close();
		}catch (SQLException e){
			System.out.println(e.getMessage());
		}
		
	}
	
}
